import React from 'react'
import { StaticQuery, Link } from 'gatsby'

import { colors } from '../utils/constants'

import rapuPng from '../../static/rapu.png'

export default ({ children }) => (
  <StaticQuery
    query={graphql`
      query HeaderQuery {
        site {
          siteMetadata {
            title
            description
          }
        }
      }
    `}
    render={data => {
      return (
        <div
          style={{
            gridArea: 'header',
            display: 'flex',
            flexFlow: 'row wrap',
            padding: '1rem 1rem 0 1rem',
            borderBottom: `5px solid ${colors.orange}`,
          }}
        >
          <div style={{ flex: '1 1 50%' }}>
            <Link to={'/'}>
              <h1 style={{ marginTop: '0', flexBasis: '100%' }}>
                {data.site.siteMetadata.title}
                <span style={{ color: colors.gray }}>.net</span>
                <img
                  style={{
                    position: 'relative',
                    width: '50px',
                    height: '25px',
                    display: 'inline-block',
                    margin: '0 0 0 15px'
                  }}
                  src={rapuPng}
                />
              </h1>
              <p style={{ color: 'black' }}>
                {data.site.siteMetadata.description}
              </p>
            </Link>
          </div>

          <div style={{ flex: '1 1 50%', textAlign: 'right', display: 'flex' }}>
            <div style={{ padding: '1rem', fontSize: '14px' }}>
              <p>
                yours truly calls himself a web developer and an unqualified
                pseudointellectual pubphilosopher
              </p>
            </div>
            <div>
                
            </div>
          </div>
        </div>
      )
    }}
  />
)
