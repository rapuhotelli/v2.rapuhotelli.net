---
date: "2019-03-30"
series: ["pub philosophy", "english"]
title: "Longing for a divine"
---

I believe religion is a crucial part of humanity, but at the same time I despise our old religions which have strongly influenced how we go on about our daily lives. This is a story of my love-hate relationship with religion and also of a little pet project I accidentally came up with to mend the "hate" part a little bit.

<!--more-->

## An outdated God

Let's get the _boohoo_ out of the way first. Somewhere around the year 2000, I was sad. The world had made it very clear I was not welcome, and if I were to stay it would be a long ride on a lonely horse. Disappointment turned into confusion, confusion into anger and finally anger into bitter resentment. That's not a place a teenager should be in without adult guidance. Every night I feared, wept and prayed. I prayed for someone to help me. I came up with a kind of prayer of my own.

I have a feeling this is how most religions are formed. Something must be able to explain the confusion, explain why humans do the evil things they do. You call someone for aid. I was – and still am – a rational nerd. I hated all church related activities in school, I hated churches and their ridiculous songs. _Of course_ everyone here must know this is all make-believe. Why do priests wear their silly costumes, why are the songs so stupid and why do they make us sing them? Yet, in my pain I was unable to reach out. Not for professionals, and not for the church. Today I know our Lutheran church does wonders for people who are close to snapping, but it was not an option. I needed rationality and comfort, and the ridiculous ancient practices of Christianity had made me lose trust in their ability to be a credible source of support.

## A spiritual helper

Again at school, I remember being introduced to _Kalevala_ and other Finnish folk stories. Most of the old Finnish folk tales are insane and describe strange pseudomagical characters doing strange things. However, I _was_ intrigued by the actual spirits and gods. Practically speaking, as guardians of some specific natural feature they are not very far off from the Japanese _shinto_ spirits. In my opinion, it's a shame that Christianity ruined the old gods. They are quickly brushed off at school and then _okay kids, get your jackets, we're going to the local church and sing a ridiclous song_.

One thing the Lutheran church did right: they stressed that everyone's beliefs are their own. Don't invade the minds of other people. This fits well into the Finnish mentality of solitude. And so, I came up with my own help. I made up a prayer-like distress call for _someone_. Much like one would write to a diary, trying to understand the world. Maybe I had made mistakes, and it would get better over time. I hated myself for being so miserable, yet I prayed for the will to live and for a better future. My little spiritual helper was somewhere between the crazy Christian God and the spirits of old, but I explicitly did not give it a name – the whole premise was to pray for _anyone who would listen_. Quite like this blog, in fact.

## Stress against rationality

Nearly 15 years pass and I find myself happy and content. One evening after a streak of disappointments I remembered my calls for aid, and I how things always turned out okay. Just like Mom had always assured me: it's not important, it'll blow over. I sent a quiet message to that special someone who had, apparently, listened for my little pleas. I am alive and well. Health is not the greatest, but I'll accept it as payment.

It got me thinking: why now? Where did the thought come from? At some point in life I had made friends – actual people I could talk to. At some point in life those friends became more prevalent and miraculously I didn't have to spend a single day without being able to speak to someone, unless I chose to. My nameless helper had served its purpose and hopefully spends its days being a friend to someone else. I understood I had given my creation much more value than I thought I had.

Every now and then I browse some magazines and tabloids for pastime and remarkably often I read some expert lamenting how the working class hyperventilates. Pretending to be busy, producing value and being rational is what defines us. There have been a couple of church-related scandals which trigger people to resign from the church in droves, including me. Today I am more mature, and I would love to support the church somehow, but I don't think there is a way back. The fundamental problem remains, the same one I understood as a small child: this church is not for us. It is for farmers and forest foragers of 300 years ago. The world has moved on. The people who resign from the church do not switch to a seemingly better alternative like you would do with politics. Instead, you are left with nothing. When life ruins you, you have nothing. You might turn to a doctor and they might give you pills to help with anxiety problems. That is our destiny.

## Speaking from experience

People seem to accept how things are, but take it from me: lacking a catalyst to channel our hopes into is bad for our feeble minds. Sometimes someone snaps and the tabloids get another sad story to tell. I strongly believe humans need _some_ kind of spiritual support structure, but none of the things we have are relatable in our ever-changing world. Hell, the world is hardly recognizable compared to what it was before the internet.

As a teenager I was very much into fantasy literatue and fantastical music. They were my primary source of happiness, being able to transport myself into faraway lands where magic didn't _need_ to be explained! It just _was_ and no character ever questioned it. One of the worlds that stuck with me was the _Forgotten Realms_ setting for the Dungeons & Dragons series of games. So many books, so many video games. One of the most interesting features in D&D were – you guessed it – the deities. Many of the books and games directly told tales where the gods of the realms were present as either physical manifestations or otherwise acted indirectly, albeit obviously. It was fascinating how the characters respected their chosen deity, their guide through life. Followers of a deity didn't _need_ a reason – the things the deities stood for provided meaning enough, and those who didn't follow a particular deity would still acknowledge them, much like our Finnish pagan gods or the _shinto kami_. They didn't force _one_ upon you – they were all there, representing different aspects of life. I nearly fell off my chair when I learned that [Mielikki](https://en.wikipedia.org/wiki/Mielikki) was incorporated _literally_ as a [goddess of forests in the Forgotten Realms setting](<https://en.wikipedia.org/wiki/Mielikki_(Forgotten_Realms)>). That is _amazing!_ (Though the American pronunciation is ridiculous.)

## Gods evolved

It's hard to express how much the fate of Mielikki inspired me. The old Finnish forest people are long gone and Mielikki has found a new life in a different world among new servants and followers – in the Forgotten Realms. The idea makes me irrationally happy, for reasons you might already guess. Many fantasy authors explained, most notably the esteemed Terry Pratchett, that the existence of a god depends on the faith of the people. A god with no followers fades away and dies. Similarly, a god is created when people make one up and start believing in their creation. This is a wonderful idea, and goes well with my... personal experience. Why not? What do we have to lose?

This brings us back to one year ago. I had just started working at a new company, full of enthusiasm and childish glee, and one early February morning I snapped a photo of the office when the morning sun shone through the empty office almost horizontally, giving it a beautiful, bright and eerie feeling. The atmosphere in that morning reflected my feelings very well, and I was happy for having been able to capture that moment in an otherwise nothing-special mobile phone photo.

Finally, we are in the present and I had just been playing a bit of _Octopath Traveler_ when I encountered a fictional character in the game. I was struck with an idea. The way she was presented in the video game was exactly what was missing from that picture from a year ago. Suddenly the picture I had snapped at the office got some _The Sixth Sense_ vibes. I got to work.

I found a cool trick to downscale the resolution of the photo to loosely resemble the art style of _Octopath Traveler_ and some kind soul on Reddit had found a way to extract character art data from the game files. The picture took me an hour to complete. Not a single pet project in my life has gone so fast from idea to completion, _and_ without any failures or backtracking. I did spend a couple of hours second-guessing myself and asking for opinions, but somehow I knew this was it.

<figure>
    <img src="./vanessa8.jpg">
</figure>

I sent the picture for printing. It arrived in 4 days, printed on a 60 cm x 40 cm canvas, just in time for my housewarming party.

<figure>
    <img src="./vanessa-canvas.jpg">
    <figcaption>Canvas in my living room</figcaption>
</figure>

For the moment she remains nameless, but you have a feeling for when she's there. Se dances in celebration for meaning of life, optimism and people who achieve their dreams, however minuscule dreams they might be. She can most often be sensed in places where people who share her celebration typically gather, such as offices with satisfied workers and startups with grand visions.

If you are at least a semi-talented artist and have an idea for an original design, please get in touch ;)

## Appendix

You might expect I would have more to say on Christian theological topics such as _secularity_. I am not, actually, very interested in philosophical debate, especially Christian, as I feel it rarely aims to solve a problem, but instead only to justify (in true Christian fashion). Christianity has no role in any of this. It is a burden of the past which still lingers due to the institution, not for what we'd expect of it. I scoff.

My little friend 15 years ago was created on the spot for myself to help with coping. Religion is one of those things that go _deep_ with people, and in true Finnish fashion, I believe introducing more spirituality to the world would only work if people were able to shut up about it and be happy with what they have. Let people have what they think they need, don't aim to populate the earth with it. However, it remains wishful thinking for as long as we have people.

All that said, I've spent so much time in exile in the internet that I feel a stronger connection to the internet culture than to our _material plane_, as D&D players would say. However, the internet is a soulless place. You don't see an e-sport player drop a silent prayer to a competitive spirit. You don't see professional communities celebrate their respective guardians. How amazing that would be! Still, nothing prevents _me_ from having my own little playground for a little make-believe.

Eventually I started running my own D&D campaign, and I did it the hard way: create and design more or less everything – except the deities. They are borrowed from Forgotten Realms. The deities will have a role in the story, but only much later. Of course, I would love for Mielikki to make an appearance, but that is up to shenanigans the players end up in. 

Maybe some day I come up with a canvas print for the next one, whatever their _domain_ would be.

